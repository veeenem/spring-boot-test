docker build -t spring-boot-test:0.0.1-SNAPSHOT .
eval $(crc oc-env)
oc login -u developer https://api.crc.testing:6443
oc registry login --skip-check
echo $(oc whoami -t) | docker login -u developer --password-stdin $(oc registry info)
echo "docker tag spring-boot-test:0.0.1-SNAPSHOT $(oc registry info)/$(oc project -q)/spring-boot-test:0.0.1-SNAPSHOT"
docker tag spring-boot-test:0.0.1-SNAPSHOT $(oc registry info)/$(oc project -q)/spring-boot-test:0.0.1-SNAPSHOT
docker push $(oc registry info)/$(oc project -q)/spring-boot-test:0.0.1-SNAPSHOT
