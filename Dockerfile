FROM openjdk:11.0.13-jre-slim-buster
ENV SPRING_PROFILES_ACTIVE=prod
COPY target/*.jar /app.jar
ENTRYPOINT ["java", "-XX:NativeMemoryTracking=summary" ,"-jar", "/app.jar"]
