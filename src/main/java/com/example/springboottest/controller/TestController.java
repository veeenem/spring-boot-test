package com.example.springboottest.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
@Slf4j
@RequiredArgsConstructor
public class TestController {

    private final RestTemplate secureRestTemplate;

    @GetMapping("/test")
    public String test() {
        log.info(secureRestTemplate.getForObject("https://my-business.itc-rubikon.ru", String.class));
        return "Hello";
    }
}
