Собираем проект:

    mvn clean package

Собираем образ:

    docker build -t spring-boot-test:0.0.1-SNAPSHOT .

Проверяем, что образ добавился в локальный репозиторий:
 
    docker images |grep spring-boot
    spring-boot-test                                                               0.0.1-SNAPSHOT                4a724f9d95ed   30 seconds ago   242MB
 
В настройках Docker Engine добавляем в json пару (после добавления перезапускаем docker):

    "insecure-registries": ["default-route-openshift-image-registry.apps-crc.testing"]

Логинимся в OpenShift:

    eval $(crc oc-env)
    oc login -u developer https://api.crc.testing:6443

Логинимся в OpenShift Container Registry (OCR):
   
    oc registry login --skip-check

Логинимся клиентом docker к OCR:

    echo $(oc whoami -t) | docker login -u developer --password-stdin $(oc registry info)

Добавляем тег к собранному образу:

    docker tag spring-boot-test:0.0.1-SNAPSHOT $(oc registry info)/$(oc project -q)/spring-boot-test:0.0.1-SNAPSHOT

Пушим образ в OCR:

    docker push $(oc registry info)/$(oc project -q)/spring-boot-test:0.0.1-SNAPSHOT

Проверяем наличие образа в OCR:

    oc get imagestream | grep spring-boot
    spring-boot-test   default-route-openshift-image-registry.apps-crc.testing/my-project/spring-boot-test   0.0.1-SNAPSHOT   46 seconds ago

Применяем файл `deploy.yaml`:

    oc apply -f deploy.yaml 
    W0201 14:35:10.443991    8879 shim_kubectl.go:55] Using non-groupfied API resources is deprecated and will be removed in a future release, update apiVersion to "route.openshift.io/v1" for your resource
    deployment.apps/spring-boot-test-deployment created
    service/spring-boot-test-service created
    route.route.openshift.io/spring-boot-test-route created

Проверяем, что под запустился:
    
    oc get pods | grep spring-boot-test
    spring-boot-test-deployment-5cffc8c574-d72nn   1/1     Running   0          92s

Проверяем роут:

    oc get routes| grep spring-boot-test
    spring-boot-test-route   spring-boot-test-route-my-project.apps-crc.testing          spring-boot-test-service   <all>                 None

Приложение доступно по адресу:

    http://spring-boot-test-route-my-project.apps-crc.testing/

Выполняем запрос:

    http://spring-boot-test-route-my-project.apps-crc.testing/api/test

Удаляем 